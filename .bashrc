# PLEAE MAKE SURE TO REMOVE THE ALEXGORE NAME AND CHANGE IT TO WHATEVER YOU WANT...

# Exit if not running interactively
[[ $- != *i* ]] && return

# Prompt configuration
PS1='\[\033[30m\][ alexgore : \h  ] '



# Aliases
alias sudo='sudo -p "$(printf "\033[1;31mPassword: \033[0;0m")"'


# Environment Variables
export PATH="$HOME/.local/bin:/usr/local/bin:$PATH"
export VIM_TRUECOLOR=1
