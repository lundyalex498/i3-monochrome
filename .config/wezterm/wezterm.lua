-- wezterm.lua configuration file

local wezterm = require 'wezterm'

local config = {}

-- Font and size
config.font = wezterm.font("uzura_font", {weight="Regular", stretch="Normal", style="Normal"})
config.font_size = 12.0

-- Custom color scheme with #fcfcfc as the background
config.colors = {
  foreground = "#3c3836",  -- Text color
  background = "#fcfcfc",  -- Background color

  cursor_bg = "#3c3836",   -- Cursor color
  cursor_border = "#3c3836",
  cursor_fg = "#fcfcfc",

  selection_bg = "#d5d5d5",  -- Selection background
  selection_fg = "#3c3836",  -- Selection foreground

  -- ANSI Colors
  ansi = {
    "#3c3836",  -- Black
    "#cc241d",  -- Red
    "#98971a",  -- Green
    "#d79921",  -- Yellow
    "#458588",  -- Blue
    "#b16286",  -- Magenta
    "#689d6a",  -- Cyan
    "#928374",  -- White
  },

  -- Bright ANSI colors
  brights = {
    "#665c54",  -- Bright Black
    "#fb4934",  -- Bright Red
    "#b8bb26",  -- Bright Green
    "#fabd2f",  -- Bright Yellow
    "#83a598",  -- Bright Blue
    "#d3869b",  -- Bright Magenta
    "#8ec07c",  -- Bright Cyan
    "#ebdbb2",  -- Bright White
  },

  -- Tab bar colors
  tab_bar = {
    background = "#ebdbb2",
    active_tab = {
      bg_color = "#fbf1c7",
      fg_color = "#3c3836",
    },
    inactive_tab = {
      bg_color = "#d5c4a1",
      fg_color = "#7c6f64",
    },
    inactive_tab_hover = {
      bg_color = "#ebdbb2",
      fg_color = "#3c3836",
      italic = true,
    },
    new_tab = {
      bg_color = "#ebdbb2",
      fg_color = "#3c3836",
    },
    new_tab_hover = {
      bg_color = "#fabd2f",
      fg_color = "#3c3836",
      italic = true,
    },
  },
}

-- Window and tab title formatting
config.window_decorations = "RESIZE"
config.window_padding = {
  left = 5,
  right = 5,
  top = 5,
  bottom = 5,
}

-- Tab bar appearance
config.enable_tab_bar = true
config.hide_tab_bar_if_only_one_tab = true

-- Key bindings
config.keys = {
  -- Split panes
  {key="d", mods="CTRL|SHIFT", action=wezterm.action.SplitHorizontal{domain="CurrentPaneDomain"}},
  {key="D", mods="CTRL|SHIFT", action=wezterm.action.SplitVertical{domain="CurrentPaneDomain"}},

  -- Close panes
  {key="w", mods="CTRL|SHIFT", action=wezterm.action.CloseCurrentPane{confirm=true}},

  -- Switch between panes
  {key="h", mods="CTRL|SHIFT", action=wezterm.action.ActivatePaneDirection("Left")},
  {key="l", mods="CTRL|SHIFT", action=wezterm.action.ActivatePaneDirection("Right")},
  {key="j", mods="CTRL|SHIFT", action=wezterm.action.ActivatePaneDirection("Down")},
  {key="k", mods="CTRL|SHIFT", action=wezterm.action.ActivatePaneDirection("Up")},

  -- Zoom pane
  {key="z", mods="CTRL|SHIFT", action=wezterm.action.TogglePaneZoomState},

  -- New tab
  {key="t", mods="CTRL|SHIFT", action=wezterm.action.SpawnTab("CurrentPaneDomain")},

  -- Close tab
  {key="q", mods="CTRL|SHIFT", action=wezterm.action.CloseCurrentTab{confirm=true}},
}

-- Enable scrollback buffer
config.scrollback_lines = 10000

-- Disable native full screen mode (macOS)
config.native_macos_fullscreen_mode = false

-- SSH domains
config.ssh_domains = {
  {
    name = "remote",
    remote_address = "user@hostname",
    username = "user",
  },
}

-- Hyperlink rules
config.hyperlink_rules = wezterm.default_hyperlink_rules()

-- Custom hyperlink format rule
table.insert(config.hyperlink_rules, {
  regex = "\\b\\w+://(?:[\\w.-]+)\\S*\\b",
  format = "$0",
  -- highlight = { underline = true },
})

-- Return the final configuration table
return config
